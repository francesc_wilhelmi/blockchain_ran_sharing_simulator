%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [] = PrintSimulationDetails(deployment, operators, users)
%PrintSimulationDetails prints simulation details
% INPUT:
%   * deployment: deployment object
%   * operators: operators object
%   * users: users object

    load('constants.mat')

    disp('--------------------------------------')
    disp('-------    SCENARIO DETAILS    -------')
    disp('--------------------------------------')

    disp([LOGS_LVL0 'Deployment information:'])
    disp([LOGS_LVL1 'Number of APs/BSs: ' num2str(deployment.nAps)])
    disp([LOGS_LVL1 'Number of STAs: ' num2str(deployment.nStas)])

    disp([LOGS_LVL0 'Operators'' information:'])
    disp([LOGS_LVL1 'Number of Operators: ' num2str(length(operators))])
    disp([LOGS_LVL1 'APs/BSs owned by each operator:'])
    for i = 1 : length(operators)
        disp([LOGS_LVL2 'Operator ' num2str(i) ' (' num2str(length(operators(i).ownedAps)) ' APs)' ':'])
        disp([LOGS_LVL3 'AP id: ' num2str(operators(i).ownedAps)])
        disp([LOGS_LVL3 'Channels per AP: ' num2str(operators(i).channelPerAp(find(operators(i).channelPerAp~=0)))])
    end

    disp([LOGS_LVL0 'Users'' information:'])
    for i = 1 : length(users)
    %    array_activation_prob = users(i).activationProbability;
        array_service_duration = users(i).averageServiceDuration;
        array_throughput_req = users(i).averageThroughputRequirement;
        array_delay_req = users(i).averageDelayRequirement;
    end
    disp([LOGS_LVL1 'Average service duration: ' num2str(mean(array_service_duration)) ' seconds'])
    disp([LOGS_LVL1 'Average throughput requirement: ' num2str(mean(array_throughput_req)/1e6) ' Mbps'])
    disp([LOGS_LVL1 'Average delay requirement: ' num2str(mean(array_delay_req)) ' seconds'])

    disp('--------------------------------------')
    
end

