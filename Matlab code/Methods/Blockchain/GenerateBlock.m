%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function block = GenerateBlock(unconfirmed_transaction_pool, ...
    t, assigned_miner, max_block_size, transaction_length, block_id)
%GenerateBlock initializes the miners of a blockchain application
% INPUT:
%   * unconfirmed_transaction_pool: pool with unconfirmed transaction objects
%   * t: time at which the transaction was generated
%   * assigned_miner: miner that generates the block
%   * max_block_size: maximum block size in bits
%   * transaction_length: lenght of each tranasction in bits
%   * block_id: identifier of the block
% OUTPUT:
%   * block: block object
    
    block = Block;
    
    block.timestamp_created = t;
    block.timestamp_mined = -1;
    block.is_mined = 0;
    %block.hash = char(randi([33 126],1,10));
    block.assigned_miner = assigned_miner;
    block.block_id = block_id;
    block.fork = 0;
    
    % Check which transactions can be included (i.e., have been propagated)         
    candidate_list_of_transactions = [];
    for i = 1 : length(unconfirmed_transaction_pool.transaction_list)
        if unconfirmed_transaction_pool.transaction_list(i).timestamp_propagated > 0
            candidate_list_of_transactions(end+1) = i; %unconfirmed_transaction_pool.transaction_list(i);
        end
    end
    
    if length(candidate_list_of_transactions)*transaction_length <= max_block_size
        block.transaction_list = unconfirmed_transaction_pool.transaction_list(candidate_list_of_transactions);
        num_transactions = length(candidate_list_of_transactions);
    else
        num_transactions = floor(max_block_size/transaction_length);
        block.transaction_list = unconfirmed_transaction_pool.transaction_list(candidate_list_of_transactions(1:num_transactions));
    end
   
%     if length(candidate_list_of_transactions)*transaction_length <= max_block_size
%         block.transaction_list = candidate_list_of_transactions;
%         num_transactions = length(candidate_list_of_transactions);
%     else
%         num_transactions = floor(max_block_size/transaction_length);
%         block.transaction_list = candidate_list_of_transactions(1:num_transactions);
%     end
    
    block.num_transactions = num_transactions;
    
end