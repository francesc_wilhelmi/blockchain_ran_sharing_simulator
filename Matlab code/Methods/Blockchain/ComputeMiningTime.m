%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function mining_time = ComputeMiningTime(mu)
%ComputeMiningTime computes the random mining time, which follows an
%exponential random variable with parameter mu
% INPUT:
%   * mu: block generation ratio
% OUTPUT:
%   * mining_time: mining time in seconds

    mining_time = exprnd(1/mu);
        
end