%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [deployment, miners] = InitializeMiners(deployment)
%InitializeMiners initializes the miners of a blockchain application
% INPUT:
%   * deployment: Deployment object
% OUTPUT:
%   * deployment: updated Deployment object
%   * miners: miners object

    % Hardcoded
    std_computational_power = 1;
    min_computational_power = 1;
    
    [~, closest_miner] = min(deployment.distApSta);
    
    for i = 1 : deployment.nAps
        miners(i).ap_id = i;
        % Assign computational power randomly
        miners(i).computational_power = 0; %max([min_computational_power, normrnd(mu, std_computational_power)]);
        % Assign users to each miner based on the distance
        miners(i).associated_stas = find(closest_miner==i);   
        for j = 1 : length(miners(i).associated_stas)
            deployment.staNearestAp(miners(i).associated_stas(j)) = i;
        end
        %miners(i).lambda_gen = mu;
    end
        
end
