%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function transaction = GenerateTransaction(type, source_id, dest_id, t, ...
    transaction_id, source_transaction_id, service_duration)
%GenerateTransaction initializes the miners of a blockchain application
% INPUT:
%   * players: list of users or operators (depending on the context)
%   * type: transaction type
%   * source_id: identifier of the transaction's source
%   * dest_id: identifier of the transaction's destination
%   * t: time at which the transaction was generated
%   * transaction_id: identifier of the transaction
%   * source_transaction_id: identifier of the original transaction's source
%   * service_duration: service duration
% OUTPUT:
%   * transaction: transaction object
       
    transaction = Transaction;
    transaction.transaction_id = transaction_id;
    transaction.timestamp_created = t;
    transaction.source_id = source_id;
    transaction.dest_id = dest_id;
    
    transaction.source_transaction_id = source_transaction_id;
    
    transaction.timestamp_propagated = -1;
    transaction.timestamp_mined = -1;
    transaction.timestamp_confirmed = -1;
    
    transaction.type = type;
    
%     if type == 1 % TRANSACTION_REQUEST_SERVICE A user requests a random service
%         transaction.service_duration = players(source_id).averageServiceDuration;
%         transaction.throughput_requirement = players(source_id).averageThroughputRequirement;
%         transaction.delay_requirement = players(source_id).averageDelayRequirement;
%         transaction.max_offered_price = players(source_id).max_price_per_unit;
%     elseif type == 2 % TRANSACTION_SERVICE_DELIVERY
%         for i = 1 : length(players)
%             if  find(players(i).ownedAps==source_id) % Find the operator owning the AP
%                 transaction.price = players(i).min_price_per_unit;
%                 transaction.operator = players(i);
%                 transaction.service_duration = service_duration;
%             end
%         end
%         
%     end
    
end