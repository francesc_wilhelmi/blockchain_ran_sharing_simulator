%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function auction_winner = ServiceAuction(deployment, ...
    transaction, operators, users, auction_approach)
%ServiceAuction executes the service auction, based on the auction mode
% INPUT:
%   * deployment: Deployment object
%   * transaction: Transaction object containing the transaction of interest
%   * operators: Operators object
%   * users: Users object
%   * auction_approach: integer with the auction mode
% OUTPUT:
%   * auction_winner: integer with the winner of the auction (id of the operator)

    load('constants.mat')

    switch auction_approach               

        case 1 % AUCTION_NEAREST_AP   % Nearest AP
            nearest_ap = deployment.staNearestAp(transaction.source_id);
            auction_winner = deployment.operatorAps(nearest_ap);
            
        case 2 % AUCTION_LOWEST_PRICE % Lowest price
            prices = zeros(1,length(operators));
            for i = 1 : length(operators)
                prices(i) = operators(i).min_price_per_unit;
            end
            [~, auction_winner] = min(prices);
            
        case 3 % AUCTION_WEIGHTED_SUM % Weighted sum
            auction_winner = deployment.staNearestAp(transaction.source_id);
        
        case 4 % AUCTION_RANDOM_SELECTION    
            auction_winner = randi(length(operators));        

        case 5 % AUCTION_MAX_UTILITY % Random selection
            utilities = zeros(1,length(operators));
            for i = 1 : length(operators)
                utilities(i) = ComputeOperatorUtility(deployment, ...
                    operators(i), users(transaction.source_id));            
            end
            [~, auction_winner] = max(utilities);

        otherwise % Unknown type
            disp('Unknown auction type')
       
    end
    
end