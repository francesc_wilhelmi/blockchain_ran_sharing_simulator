%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function utility = ComputeOperatorUtility(deployment, operator, user)
%ComputeOperatorUtility computes the utility of users provided by each operator
% INPUT:
%   * deployment: object containing the deployment
%   * operator: object containing the operator of interest
%   * user: object of the user of interest
% OUTPUT:
%   * utility: utility provided by the input operator

    available_spectrum = operator.spectrumAps(deployment.staNearestAp(user.user_id)) ...
        - sum(operator.users_requirements{deployment.staNearestAp(user.user_id)});

    utility = 1 - exp(-0.05 * available_spectrum^(user.serviceSensitivity) * ...
            (operator.min_price_per_unit)^(-user.priceSensitivity));

end