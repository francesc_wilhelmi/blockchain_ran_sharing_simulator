%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [bids] = GenerateBids(transaction_type, operators, transaction)
%ComputeUserSatisfaction initializes buyers and sellers in an auction
% INPUT:
%   * users: object containing information about UEs
%   * performance_sta: array with the performance experienced by users
%   * price_sta: array with the price paid by each user
% OUTPUT:
%   * users_satisfaction: array with satisfaction experienced by users

    bids = [];
    
    if transaction_type == 1 % TRANSACTION_REQUEST_SERVICE
        for i = 1 : length(operators)
            bid = Bid;
            bid.seller = operators(i).operator_id;
            bid.price = operators(i).min_price_per_unit;
            bid.service_duration = transaction.contract.service_duration;
            bid.offered_service = transaction.contract.throughput_requirement;
            bids = [bids bid];
        end
    elseif transaction_type == 3 % TRANSACTION_REQUEST_SPECTRUM
        for i = 1 : length(operators)
            if i ~= transaction.source_id
                bid = Bid;
                bid.seller = operators(i).operator_id;
                bid.price = operators(i).min_price_per_unit;
                bid.service_duration = transaction.contract.service_duration;
                bid.offered_service(1,:) = transaction.contract.spectrum_requested;
                bid.offered_service(2,:) = transaction.contract.ap_spectrum_requested;
                bids = [bids bid];                                
            end
        end
    end
        
          
end