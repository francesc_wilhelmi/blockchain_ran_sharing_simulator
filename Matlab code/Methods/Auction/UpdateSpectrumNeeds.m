%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [operators] = UpdateSpectrumNeeds(operators, deployment)
%UpdateSpectrumNeeds updates the operators' object with the spectrum
%required by each operator, based on users' requirements and subscriptions
% INPUT:
%   * operators: object containing operators' information
%   * deployment: object containing deployment's information
%   * op_ix: integer with the index of the involved operator
% OUTPUT:
%   * operators: updated Operators object

    for op_ix = 1 : length(operators)
        operators(op_ix).old_needs = operators(op_ix).required_spectrum;    
        for ap_ix = 1 : deployment.nAps
            for j = 1 : length(operators(op_ix).subscribed_users{ap_ix})
                if operators(op_ix).spectrumAps(ap_ix) > sum(operators(op_ix).users_requirements{ap_ix})
                    operators(op_ix).required_spectrum(ap_ix) = 0;
                else
                    operators(op_ix).required_spectrum(ap_ix) = min(1, ...
                        sum(operators(op_ix).users_requirements{ap_ix})...
                        - operators(op_ix).spectrumAps(ap_ix));
                end
            end
            % Flag of updated needs for each opeartor and AP
            if operators(op_ix).old_needs(ap_ix) < operators(op_ix).required_spectrum(ap_ix)
                operators(op_ix).updated_needs(ap_ix) = 1;
            else
                operators(op_ix).updated_needs(ap_ix) = 0;
            end
        end
        
    end

end