%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [users_satisfaction] = ComputeUserSatisfaction(users, performance_sta, price_sta)
%ComputeUserSatisfaction initializes buyers and sellers in an auction
% INPUT:
%   * users: object containing information about UEs
%   * performance_sta: array with the performance experienced by users
%   * price_sta: array with the price paid by each user
% OUTPUT:
%   * users_satisfaction: array with satisfaction experienced by users

    C = 4;  % Normalizing constant
    users_satisfaction = zeros(1,length(users));
    for i = 1 : length(users)
        if users(i).service_requested > 0
            users_satisfaction(i) = min(1, ...
                1 - exp(-C * (performance_sta(i)/users(i).service_requested)^users(i).serviceSensitivity ...
                * (1-price_sta(i))^users(i).priceSensitivity));
        else
            users_satisfaction(i) = -1;
        end
    end
          
end