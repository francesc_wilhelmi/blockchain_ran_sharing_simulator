%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [deployment] = GenerateDeployment(nStas)
%GenerateDeployment generates a Cellular or a Wi-Fi deployment
%   INPUT
%   - nStas: total number of STAs
%   OUTPUT
%   - deployment: object containing the information of the deployment

% Load deployment's configuration parameters 
load('constants.mat')
load('conf_simulation.mat')

% Generate APs
dy = 2*R*cos(pi/6);
if nRings == 0
    ApPos = [0 0];
    Ncells = 1;
elseif nRings == 1
    Ncells = 7;
    ApPos = zeros(Ncells, 2);
    ApPos(1,:) = [0 0];
    ApPos(2,:) = [0 dy];
    ApPos(3,:) = [1.5*R dy/2];
    ApPos(4,:) = [1.5*R -dy/2];
    ApPos(5,:) = [0 -dy];
    ApPos(6,:) = [-1.5*R -dy/2];
    ApPos(7,:) = [-1.5*R dy/2];
elseif nRings == 2
    Ncells = 19;
    ApPos = zeros(Ncells, 2);
    ApPos(1,:) = [0 0];
    ApPos(2,:) = [0 dy];
    ApPos(3,:) = [1.5*R dy/2];
    ApPos(4,:) = [1.5*R -dy/2];
    ApPos(5,:) = [0 -dy];
    ApPos(6,:) = [-1.5*R -dy/2];
    ApPos(7,:) = [-1.5*R dy/2];
    ApPos(8,:) = [0 2*dy];
    ApPos(9,:) = [1.5*R 1.5*dy];
    ApPos(10,:) = [3*R dy];
    ApPos(11,:) = [3*R 0];
    ApPos(12,:) = [3*R -dy];
    ApPos(13,:) = [1.5*R -1.5*dy];
    ApPos(14,:) = [0 -2*dy];
    ApPos(15,:) = [-1.5*R -1.5*dy];
    ApPos(16,:) = [-3*R -dy];
    ApPos(17,:) = [-3*R 0];
    ApPos(18,:) = [-3*R dy];
    ApPos(19,:) = [-1.5*R 1.5*dy];
else
    disp('Wrong number of rings: the maximum is 2')
end

% Generate STAs
minX = min(ApPos(:,1)) - R;
maxX = max(ApPos(:,1)) + R;
minY = min(ApPos(:,2)) - R;
maxY = max(ApPos(:,2)) + R;
for i = 1 : nStas
    % X coordinate
    StaPos(i,1) = (maxX-minX).*rand() + minX;
    % Y coordinate
    StaPos(i,2) = (maxY-minY).*rand() + minY;        
end

% Fill the object containing the deployment's information
deployment.ApPos = ApPos;
deployment.nAps = size(ApPos,1);
deployment.StaPos = StaPos;
deployment.nStas = nStas;

% Other information
deployment.defaultMcs = 4;
deployment.isCwFixed = 1;
deployment.activeStas = zeros(1,nStas);
deployment.channelStas = zeros(1,nStas);
deployment.staNearestAp = zeros(1,nStas);
deployment.apStas = zeros(1,nStas);
deployment.ccaThreshold = CCA_THRESHOLD;
deployment.TxPower = 20; % dBm

% Compute distances and signal strengths in the deployment
[deployment.distApAp, deployment.distApSta, deployment.distStaSta] = ComputeDistances(deployment);
max_d = ComputeMaxDistance(deployment);
% Correct positions of unreachable STAs
for i = 1 : nStas
    if isempty(find(deployment.distApSta(:,i) < max_d))
        while(true)
            % X coordinate
            StaPos(i,1) = (maxX-minX).*rand() + minX;
            % Y coordinate
            StaPos(i,2) = (maxY-minY).*rand() + minY;   
            deployment.StaPos = StaPos;
            [deployment.distApAp, deployment.distApSta, deployment.distStaSta] = ComputeDistances(deployment);
            if isempty(find(deployment.distApSta(:,i) < max_d))
                % repeat 
            else
                break;
            end
        end        
    end
end

% Allocate frequency resources
%    - WiFi: different operators CAN use the same freq. resources
%    - Cellular: different operators CANNOT use the same freq. resources
switch PLANNING_MODE
    %   - Channels are allocated randomly
    case 1  %WIFI_SINGLE_CHANNEL_RANDOM
        channelRange = 1 : NUM_CHANNELS_SYSTEM;
        for i = 1 : deployment.nAps            
            deployment.channelAps(i) = channelRange(randi(length(channelRange)));
        end
    %   - The same channel is allocated to all the APs               
    case 2 %WIFI_SINGLE_CHANNEL_ALL_SAME
        for i = 1 : deployment.nAps
            deployment.channelAps(i) = 1;
        end
    %   - Unknown case (display error message)
    otherwise
        disp('Error: An unknown resource planning mode was introduced');
end

[deployment.signalApAp, deployment.signalApSta, deployment.signalStaSta] = ComputeSignalReceived(deployment);
[deployment.McsMatrixAps, deployment.McsMatrixStas] = ComputeMcs(deployment);

% Assign channels to STAs (the same as closest AP)
for i = 1 : deployment.nStas
    [~, ix] = min(deployment.distApSta(:,i));
    % Assign channels
    deployment.channelStas(i) = deployment.channelAps(ix);
    % Get MCS per STA
    deployment.selectedStaMcs(i) = deployment.McsMatrixStas{ix}(i);
end