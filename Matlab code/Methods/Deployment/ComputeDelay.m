%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [delay] = ComputeDelay(deployment, technology, link_type, user_id, data_length, interference_mode)
% ComputeDelay computes the delay of a given transmission given the type of 
% deployment (e.g., WiFi, NR), the type of link (mesh, sidelink) and the 
% current status of the network
% INPUT:
%   * deployment: object containing information about the deployment
%   * technology: underlying technology (WiFi vs NR)
%   * link_type: type of link considered
%   * user_id: identifier of the user of interest
%   * data_length: length of the data to be transmitted
%   * interference_mode: interference mode considered
% OUTPUT:
%   * delay: communication delay in seconds


    % Use the same channel as the associated miner to compute the delay to reach the miner
    if user_id > 0
        deployment.channelStas(user_id) = deployment.channelAps(deployment.staNearestAp(user_id));
        deployment.activeStas(user_id) = 1; % Enable the STA to compute the delay  
    end

    % Compute the expected number of hops (approximation)
    average_number_of_hops = deployment.nAps / mean(sum(deployment.signalApAp>deployment.ccaThreshold));
                
    switch technology
        
        % WiFi
        case 1 %WIFI
             if link_type == 1 %ACCESS_NETWORK                 
                [throughput_sta,~] = ComputeThroughputBianchi(deployment, 1, interference_mode);
                delay = data_length / throughput_sta(user_id);                 
             elseif link_type == 2 %P2P_NETWORK             
                [~,throughput_ap] = ComputeThroughputBianchi(deployment, 2, interference_mode);
                % Compute the delay
                delay = average_number_of_hops * data_length / mean(throughput_ap);                                     
             end
        
        % NR
        case 2 %NR            
            if link_type == 1       % ACCESS_NETWORK
                delay = average_number_of_hops * 0.01;
            elseif link_type == 2   % P2P_NETWORK
                delay = average_number_of_hops * 0.01;
            end
        
        % Unknown    
        otherwise
            disp(['Unknown technology type. Current available technologies: WIFI (' num2str(1) ') and NR (' num2str(2) ')'])
       
    end   
    
end

