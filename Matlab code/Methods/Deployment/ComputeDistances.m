%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************s
function [distApAp, distApSta, distStaSta] = ComputeDistances(deployment)
%ComputeDistances computes distances between devices in a wireless deployment
% INPUT:
%   * deployment: Deployment object
% OUTPUT:
%   * distApAp: matrix with distance among APs
%   * distApSta: matrix with distance among APs and STAs
%   * distStaSta: matrix with distance among STAs
 
distApAp = zeros(deployment.nAps, deployment.nAps);
distApSta = zeros(deployment.nAps, deployment.nStas);
distStaSta = zeros(deployment.nStas, deployment.nStas);

% Distance among APs and APs/STAs
for i = 1 : deployment.nAps
    
    for j = 1 : deployment.nAps
        distApAp(i,j) = sqrt((deployment.ApPos(i,1)-deployment.ApPos(j,1))^2 ...
            + (deployment.ApPos(i,2) - deployment.ApPos(j,2))^2);
    end
    
    for n = 1 : deployment.nStas
        distApSta(i,n) = sqrt((deployment.ApPos(i,1)-deployment.StaPos(n,1))^2 ...
            + (deployment.ApPos(i,2) - deployment.StaPos(n,2))^2);
    end   
    
end

% Distance among STAs
for i = 1 : deployment.nStas
    for n = 1 : deployment.nStas
        distStaSta(i,n) = sqrt((deployment.StaPos(i,1)-deployment.StaPos(n,1))^2 ...
            + (deployment.StaPos(i,2) - deployment.StaPos(n,2))^2);
    end     
end

end