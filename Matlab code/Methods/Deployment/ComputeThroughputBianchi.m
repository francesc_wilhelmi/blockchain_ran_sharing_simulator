%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [throughput_stas, throughput_ap] = ComputeThroughputBianchi(deployment, link_type, interference_mode)
%ComputeThroughput computes the throughput obtained by all the STAs and APs
% in a Wi-Fi deployment
% INPUT:
%   * deployment: Deployment object
%   * link_type: integer indicating the type of link used in wireless comm.
%   * interference_mode: integer indicating the way interference is considered
% OUTPUT:
%   * throughput_stas: throughput obtained by STAs
%   * throughput_ap: throughput obtained by APs

    % Initialize variables
    throughput_stas = zeros(1,deployment.nStas);
    throughput_ap = zeros(1,deployment.nAps);
    
    if link_type == 1 % technology=WIFI - link_type=ACCESS_NETWORK
        
        % Activate all the STAs for the "worst-case" scenario
        if interference_mode == 1
            for i = 1 : deployment.nStas
                deployment.activeStas(i) = 1;
                deployment.channelStas(i) = 1;
            end
        end
        
        % Iterate for each STA in the deployment        
        for i = 1 : deployment.nStas      
            mcs_list = [];
            if deployment.activeStas(i)
                % Check number of overlapping APs and STAs
                nAps = 0;
                nStas = 0;
                for j = 1 : deployment.nAps
                    if deployment.signalApSta(j,i)> deployment.ccaThreshold && ...
                         deployment.channelStas(i) == deployment.channelAps(j)
                        nAps = nAps + 1;
                    end
                end
                for j = 1 : deployment.nStas
                    if deployment.signalStaSta(i,j)>deployment.ccaThreshold && ...
                         deployment.channelStas(i) == deployment.channelStas(j) && ...
                         deployment.activeStas(j)
                        nStas = nStas + 1;
                        mcs_list = [mcs_list deployment.selectedStaMcs(j)];
                    end
                end                
                average_mcs = max(1, mean(mcs_list));
                % Compute the throughput of the STA of interest
                throughput_stas(i) = DCFmodel11ax(nAps, nStas, ceil(average_mcs), deployment.isCwFixed)/(nStas+nAps);
            else
                throughput_stas(i) = 0;
            end             
        end
        
        % Iterate for each AP in the deployment
        for i = 1 : deployment.nAps    
            % Check number of overlapping APs and STAs
            nAps = sum(deployment.signalApAp(i,:)>deployment.ccaThreshold);
            nStas = 0;
            for j = 1 : deployment.nStas
                if deployment.activeStas(j) && deployment.signalApSta(i,j)>deployment.ccaThreshold && ...
                     deployment.channelAps(i) == deployment.channelStas(j)
                    nStas = nStas + 1;
                end
            end
            % nStas = sum(deployment.signalApSta(i,:)>CCA_THRESHOLD);
            % Compute the throughput of the AP of interest
            throughput_ap(i) = DCFmodel11ax(nAps, nStas, deployment.defaultMcs, deployment.isCwFixed)/(nStas+nAps);
        end
        
    elseif link_type == 2 % technology=WIFI - link_type=P2P_NETWORK
        % Iterate for each AP in the deployment
        for i = 1 : deployment.nAps    
            % Check number of nearby APs (all use the same channel in the mesh)
            nAps = sum(deployment.signalApAp(i,:)>deployment.ccaThreshold);
            % Compute the throughput of the AP of interest
            throughput_ap(i) = DCFmodel11ax(nAps, 0, deployment.defaultMcs, deployment.isCwFixed)/(nAps);
        end
    end
    
end

