%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [throughput_users, satisfaction_users, activation_users, period_durations, requests_users, load_aps, required_load_aps] = ...
    UpdatePerformanceStatistics(deployment, operators, users, ue_required_spectrum, throughput_users, ...
    satisfaction_users, activation_users, period_durations, requests_users, load_aps, required_load_aps, active_stas, price_sta, t)
%UpdatePerformanceStatistics updates performance statistics of users
% INPUT:
%   * 
% OUTPUT:
%   * 

    % Compute and store the throughput until this moment
    [throughput_sta, spectrum_sta, load_per_ap, required_load_per_ap] = ComputeCapacity(deployment, operators);    
    throughput_users(end+1,:) = throughput_sta;    
    satisfaction_sta = ComputeUserSatisfaction(users, spectrum_sta, price_sta); 
    satisfaction_users(end+1,:) =  satisfaction_sta; % (spectrum_sta/ue_required_spectrum)
    activation_users(end+1,:) = active_stas;        
    period_durations(end+1) = t;
    requests_users(end+1,:) = ue_required_spectrum;
    load_aps(end+1,:) = load_per_ap;
    required_load_aps(end+1,:) = required_load_per_ap;
        
end