%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [oh_an,oh_p2p,oh_p2p_w_f,timestamp_bc] = UpdateOverheadBlockchain(oh_an, oh_p2p, ...
    oh_p2p_w_f, timestamp_bc, oh_an_aux, oh_p2p_aux, oh_p2p_w_f_aux, t)
%UpdateOverheadBlockchain updates the statistics related to spectrum sharing
% INPUT:
%   * 
% OUTPUT:
%   * 

    oh_an(end+1) = oh_an_aux;
    oh_p2p(end+1) = oh_p2p_aux;
    oh_p2p_w_f(end+1) = oh_p2p_w_f_aux;
    timestamp_bc(end+1) = t;  
        
end