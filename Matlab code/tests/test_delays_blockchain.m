path_file = 'Output/output_4_2_5_5_3000_2_1.mat';
load(path_file);

propagation_delay = [];
mining_delay = [];
confirmation_delay = [];
e2e_delay = [];
% SERVICE BC
for i = 1 : length(service_bc.mined_block_list)        
    for j = 1 : length(service_bc.mined_block_list(i).transaction_list)        
        ts_created = service_bc.mined_block_list(i).transaction_list(j).timestamp_created;
        ts_propagated = service_bc.mined_block_list(i).transaction_list(j).timestamp_propagated;
        ts_mined = service_bc.mined_block_list(i).transaction_list(j).timestamp_mined;
        ts_confirmed = service_bc.mined_block_list(i).transaction_list(j).timestamp_confirmed; 
        ts_served = service_bc.mined_block_list(i).transaction_list(j).timestamp_served; 
        if ts_propagated > 0
            propagation_delay = [propagation_delay ts_propagated-ts_created];
        end        
        if ts_mined > 0
            mining_delay = [mining_delay ts_mined-ts_created];
        end
        if ts_confirmed > 0
            confirmation_delay = [confirmation_delay ts_confirmed-ts_created];
        end   
        if ts_served > 0
            e2e_delay = [e2e_delay ts_served-ts_created];
        end   
    end
end
disp('SUMMARY SERVICE BC:')
disp([' + Propagation delay (' num2str(length(propagation_delay)) ' samples): ' num2str(mean(propagation_delay)) ' s'])
disp([' + Mining delay (' num2str(length(mining_delay)) ' samples): ' num2str(mean(mining_delay)) ' s'])
disp([' + Confirmation delay (' num2str(length(confirmation_delay)) ' samples): ' num2str(mean(confirmation_delay)) ' s'])
disp([' + E2E delay (' num2str(length(e2e_delay)) ' samples): ' num2str(mean(e2e_delay)) ' s'])

propagation_delay = [];
mining_delay = [];
confirmation_delay = [];
e2e_delay = [];
% SPECTRUM BC
for i = 1 : length(spectrum_bc.mined_block_list)        
    for j = 1 : length(spectrum_bc.mined_block_list(i).transaction_list)        
        ts_created = spectrum_bc.mined_block_list(i).transaction_list(j).timestamp_created;
        ts_propagated = spectrum_bc.mined_block_list(i).transaction_list(j).timestamp_propagated;
        ts_mined = spectrum_bc.mined_block_list(i).transaction_list(j).timestamp_mined;
        ts_confirmed = spectrum_bc.mined_block_list(i).transaction_list(j).timestamp_confirmed;  
        ts_served = spectrum_bc.mined_block_list(i).transaction_list(j).timestamp_served;
        if ts_propagated > 0
            propagation_delay = [propagation_delay ts_propagated-ts_created];
        end        
        if ts_mined > 0
            mining_delay = [mining_delay ts_mined-ts_created];
        end
        if ts_confirmed > 0
            confirmation_delay = [confirmation_delay ts_confirmed-ts_created];
        end    
        if ts_served > 0
            e2e_delay = [e2e_delay ts_served-ts_created];
        end   
    end
end
disp('SUMMARY SPECTRUM BC:')
disp([' + Propagation delay (' num2str(length(propagation_delay)) ' samples): ' num2str(mean(propagation_delay))])
disp([' + Mining delay (' num2str(length(mining_delay)) ' samples): ' num2str(mean(mining_delay))])
disp([' + Confirmation delay (' num2str(length(confirmation_delay)) ' samples): ' num2str(mean(confirmation_delay))])
disp([' + E2E delay (' num2str(length(e2e_delay)) ' samples): ' num2str(mean(e2e_delay)) ' s'])