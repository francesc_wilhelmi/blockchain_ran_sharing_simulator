%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
   
% Initialize blockchain objects and pools of transactions
service_bc = Blockchain;                           % Service blockchain
spectrum_bc = Blockchain;                          % Spectrum blockchain
unconfirmed_transaction_pool = TransactionPool;    % Pool of unconfirmed service req.
unconfirmed_spectrum_pool = TransactionPool;       % Pool of unconfirmed spectrum req.  
temp_transaction_pool = TransactionPool;           % Aux object to store transactions temporarily

% Initialize users' statistics
ue_required_spectrum = zeros(1,deployment.nStas);
for i = 1 : deployment.nStas    
    users(i).service_requested = 0; % Current service requested
    ue_required_spectrum(i) = users(i).averageThroughputRequirement;
end
deployment.activeStas = zeros(1,deployment.nStas);

services_information = [];      % Main object to keep track of the steps followed by transactions [SERVICE BC]
spectrum_information = [];      % Main object to keep track of the steps followed by transactions [SPECTRUM BC]

previous_t = 0;                 % Auxiliary variable to keep track of monitoring periods' duration
next_block_timeout = UNDEFINED; % Auxiliary variable for mining block timeouts (users BC)
next_block_timeout_spectrum = UNDEFINED; % Auxiliary variable for mining block timeouts (spectrum BC)
transaction_counter = 1;        % Counter of transactions
service_counter = 1;            % Counter of UE requested services
spectrum_counter = 1;           % Counter of MNO requested services
block_id = 1;                   % Identifier of the current service block
spectrum_block_id = 1;          % Identifier of the current spectrum block
mining_active = 0;              % Flag to indicate whether mining is active (1) or not (0)
mining_spectrum_active = 0;     % Flag to indicate whether mining is active (1) or not (0)
total_transactions = 0;         % Total number of transactions
dropped_requests = 0;           % Total number of dropped requests (users)
dropped_spectrum_requests = 0;  % Total number of dropped requests (operators)
mining_times = 0;               % Times at which mining is performed
already_requested_spectrum = zeros(1,deployment.nAps);
new_service_started = 0;

% Statistics
throughput_users = zeros(1,deployment.nStas);   % Users' throughput
satisfaction_users = zeros(1,deployment.nStas); % Users' satisfaction
activation_users = zeros(1,deployment.nStas);   % Times at which users are activated
requests_users = zeros(1,deployment.nStas);     % Temporal bandwidth requested by users
price_sta = zeros(1,deployment.nStas);          % Price per bandwidth paid by users
load_aps = zeros(1,deployment.nAps);            % Total load of each AP
required_load_aps = zeros(1,deployment.nAps);   % Total load of each AP
period_durations = 0;                           % Periods used to make performance measurements
total_mined_block_propagated = 0;
total_blocks = 0;                               % Total number of blocks
forked_blocks = [];                             % List of forked service blocks
forked_blocks_spectrum = [];                    % List of forked spectrum blocks

spectrum_ownership = cell(1,length(operators));
spectrum_periods = 0;