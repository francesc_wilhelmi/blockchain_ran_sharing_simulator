%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
classdef Transaction
    % Transaction object to be used in a blockchain scenario
    %   Transactions define the information required to enable service
    %   provisioning and RAN sharing in a blockchain-enabled ecosystem
    
    properties
        transaction_id
        source_transaction_id
        timestamp_created
        timestamp_propagated
        timestamp_added_to_block
        timestamp_mined
        timestamp_confirmed
        timestamp_served
        type
        source_id
        dest_id       
        contract        
    end
       
    methods(Static)
        function [] = PrintTransactionInfo()
            load('constants.mat')
            disp(['Transaction ' num2str(obj.transaction_id) ' information:'])
            disp([LOG_LVL1 'Is confirmed: ' num2str(obj.is_confirmed)])
            disp([LOG_LVL1 'Timestamp created: ' num2str(obj.timestamp_created)])
            disp([LOG_LVL1 'Timestamp received at P2P network: ' num2str(obj.timestamp_received_p2p)])
            disp([LOG_LVL1 'Timestamp validated: ' num2str(obj.timestamp_confirmed)])
            disp([LOG_LVL1 'Transaction details: '])
            disp([LOG_LVL2 'source id: ' num2str(obj.request_details.source_id)])
            disp([LOG_LVL2 'Throughput requirement: ' num2str(obj.request_details.throughput_requirement)])
            disp([LOG_LVL2 'Delay requirement: ' num2str(obj.request_details.delay_requirement)])
            disp([LOG_LVL2 'Max price to pay: ' num2str(obj.request_details.max_price_to_pay)])
        end
    end
    
end

