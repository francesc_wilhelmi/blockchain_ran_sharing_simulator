%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
classdef Blockchain
    % Blockchain object that maintains a distributed ledger of transactions
    %  The blockchain contains mined blocks, which contain valid transactions
  
    properties
        mined_block_list = [];
        block_list = [];
        mined_block_count = 0
        block_count = 0        
        total_size = 0
        num_forks = 0
    end
    
    methods
        
        % Update size of the blockchain
        function obj = UpdateSize(obj)
            %UNTITLED7 Construct an instance of this class
            %   Detailed explanation goes here
            bc_length = 0;
            for i = 1 : length(obj.block_list)
                bc_length = obj.block_list(i).block_size;
            end
            obj.total_size = bc_length;
        end
        
        % Add block (not mined)
        function obj = AddBlock(obj, block, t)
            if ~isempty(obj.block_list)
                obj.block_list(end+1) = block;
            else
                obj.block_list = block;
            end
            obj.block_count = obj.block_count + 1;
            %obj.block_list(end).timestamp_mined = t; 
            obj.block_list(end).block_id = block.block_id;
        end
        
        % Mine block (add a new one)
        function obj = MineBlock(obj, block, t)
            if ~isempty(obj.mined_block_list)
                obj.mined_block_list(end+1) = block;
            else
                obj.mined_block_list = block;
            end
            obj.mined_block_count = obj.mined_block_count + 1;
            obj.mined_block_list(end).timestamp_mined = t; 
            obj.mined_block_list(end).is_mined = 1; 
            obj.mined_block_list(end).block_id = block.block_id;
        end
                
        % Remove the target block from list of unmined blocks
        function obj = RemoveBlock(obj, id)
            obj.block_list(id) = [];
        end
        
        function obj = RemoveMinedBlock(obj, id)
            obj.mined_block_list(id) = [];
        end
        
        % Print information of a block in the blockchain
        function [] = PrintBlockInfo(block_id)
            obj.block_list(block_id).PrintBlockInfo;
        end
        
    end
  
end

