%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************

function [] = RunSimulation(deployment, operators, users, miners, ...
    SERVICE_AUCTION_MODE, SPECTRUM_AUCTION_MODE, LAMBDA, T_WAIT, MAX_BLOCK_SIZE, RAND_DEPL)

tic % Measure the time of a single simulation

% Load constants and environment variables
load('constants')
load('conf_simulation')

% Initialize variables to be used within the simulation
initialize_variables

MAX_BLOCK_SIZE_SPECTRUM = TRANSACTION_LENGTH*block_size_spectrum; 

% Create the logs file
logs_file = 'logs.txt'; 
if exist(logs_file, 'file')==2
  delete(logs_file);
end

% Generate the first service request
events.time = exprnd(1/LAMBDA);
events.type = EVENT_NEW_TRANSACTION;
events.subtype = TRANSACTION_REQUEST_SERVICE;
inactive_stas = find(deployment.activeStas==0);
events.source_id = inactive_stas(randi(length(inactive_stas)));
events.transaction_id = 1;
events.service_ix = 0;

% Write simulation's info
WriteDeploymentDetails(LOGS_ENABLED, logs_file, deployment);
WriteAuctionDetails(LOGS_ENABLED, logs_file, users, operators);
WriteBlockchainDetails(LOGS_ENABLED, logs_file, miners);

% Run the simulation
t = 0;
WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL0 'SIMULATION STARTED']);
while t < sim_time && ~isempty(events.time) % && total_transactions < NUM_TRANSACTIONS_SIMULATION)
    
    % Jump into the next event
    t = events.time(1);
    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL1 'EVENT of type ' num2str(events.type(1)) ' from source ' num2str(events.source_id(1))]);

    % Check which is the next event (user enters / user leaves)
    switch events.type(1)

        %%%% NEW REQUEST GENERATED: A new transaction is generated
        case EVENT_NEW_TRANSACTION % 1

            % Identify source of transaction and subtype (service request or service delivery)
            source_transaction = events.source_id(1);
            transaction_type = events.subtype(1);
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 ...
                'New transaction (type: ' num2str(transaction_type) ') from ' num2str(source_transaction)]);
            % Generate the transaction
            new_transaction = GenerateTransaction(transaction_type, ...
                source_transaction, UNDEFINED, t, transaction_counter, UNDEFINED, UNDEFINED);            
            % Compute the delay to reach the P2P network (propagate the transaction)
            delay_sta_miner = ComputeDelay(deployment, WIFI, ...
                ACCESS_NETWORK, source_transaction, HEADER_LENGTH+TRANSACTION_LENGTH, INTERFERENCE_MODE);
            delay_p2p = ComputeDelay(deployment, WIFI, P2P_NETWORK, ...
                deployment.staNearestAp(source_transaction), HEADER_LENGTH+TRANSACTION_LENGTH, INTERFERENCE_MODE);
            % Define the contract inside the transaction
            if transaction_type == TRANSACTION_REQUEST_SERVICE
                service_request = ServiceRequest;        
                service_request.service_duration = users(source_transaction).averageServiceDuration;
                service_request.throughput_requirement = users(source_transaction).averageThroughputRequirement;
                service_request.delay_requirement = users(source_transaction).averageDelayRequirement;
                service_request.max_offered_price = users(source_transaction).max_price_per_unit;
                new_transaction.contract = service_request;    
                % Generate the next user request  
                if transaction_type == TRANSACTION_REQUEST_SERVICE
                    new_event = CreateEvent(EVENT_NEW_TRANSACTION, ...
                        TRANSACTION_REQUEST_SERVICE, t + exprnd(1/LAMBDA), ...
                        randi(nStas), UNDEFINED, 0);
                    events = AddEvents(events, new_event); 
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 ...
                        'Scheduled a new UE (' num2str(new_event.source_id) ') request at t=' num2str(new_event.time)]);
                end 
            elseif transaction_type == TRANSACTION_REQUEST_SPECTRUM
                % Identify source of transaction and subtype
                ap_spectrum_requested = events.transaction_id(1);   
                spectrum_requested = events.service_ix(1);     
                % Edit spectrum lease details (needed spectrum, duration)
                max_time_per_channel = zeros(1, deployment.nAps);
                spectrum_per_channel = zeros(1, deployment.nAps);
                for i = 1 : deployment.nAps
                    if ~isempty(operators(source_transaction).users_service_time{i})
                        max_time_per_channel(i) = max(operators(source_transaction).users_service_time{i});
                        spectrum_per_channel(i) = sum(operators(source_transaction).users_requirements{i});
                    end
                end                
                spectrum_request = SpectrumRequest;
                spectrum_request.max_offered_price = operators(source_transaction).min_price_per_unit;
                spectrum_request.spectrum_requested = spectrum_requested;
                spectrum_request.ap_spectrum_requested = ap_spectrum_requested;
                spectrum_request.service_duration = max_time_per_channel(ap_spectrum_requested);
                spectrum_request.operator = zeros(1, deployment.nAps);        % To store which operator leased spectrum in each AP
                spectrum_request.spectrum_shared = zeros(1, deployment.nAps); % To store the amount of leased spectrum in each AP
                new_transaction.contract = spectrum_request;                            
            end         
            % Add the new transaction to the pool of unconfirmed transactions
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Adding transaction (id ' ...
                num2str(new_transaction.transaction_id) ') to temp pool (current pool size: ' ...
                num2str(length(temp_transaction_pool.transaction_list)) ')' ]);            
            temp_transaction_pool = AddTransaction(temp_transaction_pool, new_transaction, 0, t, 0);  
            % Schedule a new event (propagate the transaction)
            new_event = CreateEvent(EVENT_TRANSACTION_PROPAGATED, ...
                transaction_type, t + delay_sta_miner + delay_p2p, ...
                source_transaction, new_transaction.transaction_id, service_counter);
            events = AddEvents(events, new_event);  
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Time to reach P2P network: ' num2str(new_event.time)]);
            % Increase transactions counter
            transaction_counter = transaction_counter + 1;
            % Remove the current event from the list
            events = RemoveEvents(events, 1);                                    
                       
        %%%% TRANSACTION PROPAGATED: The transaction is available to all the P2P BC nodes
        case EVENT_TRANSACTION_PROPAGATED  % 2

            % Find the id of the transaction in the pool
            transaction_id = events.transaction_id(1);
            ix_in_pool = -1;
            for i = 1 : length(temp_transaction_pool.transaction_list)
                if temp_transaction_pool.transaction_list(i).transaction_id == transaction_id
                    ix_in_pool = i;
                end
            end
            % Save the propagated transaction
            propagated_transaction = temp_transaction_pool.transaction_list(ix_in_pool);
            % Add propagation timestamp to the propagated transaction
            propagated_transaction.timestamp_propagated = t;
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 ...
                'Id transaction propagated: ' num2str(transaction_id) ' (ix in temp pool ' num2str(ix_in_pool) ')']);
            % Check if the queue is full and schedule a new re-transmission
            dropped = 0;
            if length(unconfirmed_transaction_pool.transaction_list) > queue_length
                % Discard the request
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'Transaction dropped']);
                dropped_requests = dropped_requests + 1;    
                dropped = 1;
                if RETRANSMISSIONS_ENABLED % Re-schedule dropped transactions   
                    new_event = CreateEvent(EVENT_NEW_TRANSACTION, ...
                        events.subtype(1), t + exprnd(1/LAMBDA), ...
                        propagated_transaction.source_id, UNDEFINED, 0);
                    events = AddEvents(events, new_event); 
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Scheduled a new UE (' ...
                        num2str(new_event.source_id) ') request at t=' num2str(new_event.time)]);
                end
            end
            
            % NEW: DIFFERENTIATE BETWEEN USERS AND OPERATORS REQUESTS
            switch events.subtype(1) 
                % USER REQUESTS
                case {TRANSACTION_REQUEST_SERVICE, TRANSACTION_SERVICE_BID}                  
                    % Update the status of the deployment (disable the STA until the service starts)
                    deployment = UpdateDeploymentStatus(deployment, ...
                        events.source_id(1), 0, UNDEFINED, UNDEFINED);
                    % Check if the queue is full
                    if ~dropped
                        % Add transaction to the pool
                        unconfirmed_transaction_pool = AddTransaction(unconfirmed_transaction_pool, ...
                            propagated_transaction, 0, t, TRANSACTION_LENGTH, 0);
                        if ~isempty(unconfirmed_transaction_pool.transaction_list)                                   
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Transaction propagated: ']);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transaction id: ' ...
                                num2str(unconfirmed_transaction_pool.transaction_list(end).transaction_id) ...
                                ' (ix in pool = ' num2str(length(unconfirmed_transaction_pool.transaction_list)) ')']);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Type: ' num2str(unconfirmed_transaction_pool.transaction_list(end).type)]);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Source id: ' num2str(unconfirmed_transaction_pool.transaction_list(end).source_id)]);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Dest id: ' num2str(unconfirmed_transaction_pool.transaction_list(end).dest_id)]);
                        end
                        % Check if mining is possible, compute the time for mining the block, and schedule an event
                        if ( ~mining_active && (unconfirmed_transaction_pool.total_size >= MAX_BLOCK_SIZE || ...
                                (events.type(1) == EVENT_BLOCK_TIMEOUT && ~isempty(unconfirmed_transaction_pool.transaction_list))) )
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Mining is possible (users BC)']);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transactions in users'' queue: ' ...
                                num2str(unconfirmed_transaction_pool.total_size/TRANSACTION_LENGTH)]);
                            % Create a mining event
                            new_event = CreateEvent(EVENT_START_MINING, events.subtype(1), ...
                                t+rand()/100000, UNDEFINED, UNDEFINED, UNDEFINED);                                    
                            events = AddEvents(events, new_event);
                        else
                            % Create a timeout for generating the block (add it later to avoid conflicts with events)
                            if next_block_timeout == UNDEFINED && ~mining_active
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Current next_block_timeout = ' num2str(next_block_timeout)]);
                                next_block_timeout = t + T_WAIT;
                                timeout_event = CreateEvent(EVENT_BLOCK_TIMEOUT, events.subtype(1), ...
                                    next_block_timeout, source_transaction, UNDEFINED, service_counter); 
                                events = AddEvents(events, timeout_event);
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Timeout for generating a block (users): ' num2str(timeout_event.time)]);
                            end                             
                        end
                    end
                % OPERATORS REQUESTS
                case {TRANSACTION_REQUEST_SPECTRUM, TRANSACTION_SPECTRUM_BID}
                    % Check if the queue is full
                    if ~dropped
                        % Add transaction to the pool
                        unconfirmed_spectrum_pool = AddTransaction(unconfirmed_spectrum_pool, ...
                            propagated_transaction, 0, t, TRANSACTION_LENGTH, 0);
                        if ~isempty(unconfirmed_spectrum_pool.transaction_list)                                   
                            % Update the timestamp at which the transaction was received by the P2P network
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Transaction propagated: ']);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transaction id: ' ...
                                num2str(unconfirmed_spectrum_pool.transaction_list(end).transaction_id) ...
                                ' (ix in pool = ' num2str(length(unconfirmed_spectrum_pool.transaction_list)) ')']);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Type: ' num2str(unconfirmed_spectrum_pool.transaction_list(end).type)]);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Source id: ' num2str(unconfirmed_spectrum_pool.transaction_list(end).source_id)]);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Dest id: ' num2str(unconfirmed_spectrum_pool.transaction_list(end).dest_id)]);
                        end
                        % Check if mining is possible, compute the time for mining the block, and schedule an event
                        if ( ~mining_spectrum_active && (unconfirmed_spectrum_pool.total_size >= MAX_BLOCK_SIZE_SPECTRUM  ))  %|| ...
                               %(events.type(1) == EVENT_BLOCK_TIMEOUT && ~isempty(unconfirmed_spectrum_pool.transaction_list))) )
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Mining is possible (operators'' BC)']);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transactions in queue: ' ...
                                num2str(unconfirmed_spectrum_pool.total_size/TRANSACTION_LENGTH)]);
                            % Create mining event
                            %mining_spectrum_active = 1;
                            new_event = CreateEvent(EVENT_START_MINING, events.subtype(1), ...
                                t+rand()/100000, UNDEFINED, UNDEFINED, UNDEFINED);                                    
                            events = AddEvents(events, new_event);
                        else
                            % Create a timeout for generating the block (add it later to avoid conflicts with events)
                            if next_block_timeout_spectrum == UNDEFINED && ~mining_spectrum_active
                                next_block_timeout_spectrum = t + T_WAIT;
                                timeout_event = CreateEvent(EVENT_BLOCK_TIMEOUT, events.subtype(1), ...
                                    next_block_timeout_spectrum, source_transaction, UNDEFINED, spectrum_counter); 
                                events = AddEvents(events, timeout_event);
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Timeout for generating a block (spectrum): ' num2str(timeout_event.time)]);
                            end                             
                        end
                    end
                    
                % UNKNOWN CASE    
                otherwise
                    disp('Unknown event subtype in state EVENT_TRANSACTION_PROPAGATED')      
                    
            end
            
            % Remove the transaction from temp pool
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Current size of the temp pool: ' num2str(length(temp_transaction_pool.transaction_list))]);
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Removing transaction from temp pool: ' num2str(transaction_id)]);
            temp_transaction_pool = RemoveTransactions(temp_transaction_pool, transaction_id, TRANSACTION_LENGTH);
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Updated size of the temp pool: ' num2str(length(temp_transaction_pool.transaction_list))]);
            % Remove the current event from the list
            events = RemoveEvents(events, 1);

        %%%% START MINING: Mining starts (block full or timeout)
        case {EVENT_BLOCK_TIMEOUT, EVENT_START_MINING} % 6 / 7
            
            % Check timeout
            if events.type(1) == EVENT_BLOCK_TIMEOUT
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Block timeout']);
                if events.subtype(1) == TRANSACTION_REQUEST_SERVICE
                    next_block_timeout = UNDEFINED; 
                elseif events.subtype(1) == TRANSACTION_REQUEST_SPECTRUM
                    next_block_timeout_spectrum = UNDEFINED; 
                end
            end
            
            % Differentiate between service and spectrum BCs
            % + SERRVICE BC
            if events.subtype(1) == TRANSACTION_REQUEST_SERVICE
                mining_active = 1;
                % If FORKS_ENABLED=1, each miner generates a block. Otherwise, a random miners is selected.  
                if ~FORKS_ENABLED, assigned_miner = randi(length(miners)); end % Assign miner randomly
                delays_aux = [];
                for i = 1 : length(miners) 
                    if FORKS_ENABLED || (~FORKS_ENABLED && assigned_miner == i)
                        delay_mining = ComputeMiningTime(mu);
                        delays_aux = [delays_aux delay_mining];
                        block = GenerateBlock(unconfirmed_transaction_pool, t, i, MAX_BLOCK_SIZE, TRANSACTION_LENGTH, block_id);                                    
                        service_bc = AddBlock(service_bc, block, t);
                        service_bc.block_list(end).time_to_mine = delay_mining;    
                        new_event = CreateEvent(EVENT_BLOCK_MINED, events.subtype(1), ...
                            t+delay_mining, i, block_id, UNDEFINED); 
                        events = AddEvents(events, new_event); % Add new event to the queue
                    end
                end 
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'A service block with id ' num2str(block.block_id) ' is ready to be mined']);
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Mining delay = ' num2str(min(delays_aux))]);
                % Store the information about the service
                for j = 1 : length(block.transaction_list)
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transaction id: ' num2str(block.transaction_list(j).transaction_id)]);
                    service_bc.block_list(end).transaction_list(j).timestamp_added_to_block = t;
                end
                % Reset the block timeout
                if events.type(1) ~= EVENT_BLOCK_TIMEOUT % Remove the timeout if not used
                    event_number = find(events.time == next_block_timeout);                                
                    events = RemoveEvents(events,event_number);
                    % Create another timeout if there are unconfirmed transactions in the pool
                    if length(unconfirmed_transaction_pool.transaction_count) > 1
                        next_block_timeout = t + T_WAIT;                                    
                        timeout_event = CreateEvent(EVENT_BLOCK_TIMEOUT, events.subtype(1), ...
                                next_block_timeout, source_transaction, UNDEFINED, service_counter);
                        events = AddEvents(events, timeout_event);
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Timeout for generating a block: ' num2str(timeout_event.time)]);
                    end                                 
                end            
                % Increase the block id count
                block_id = block_id + 1;   
            % + SPECTRUM BC    
            elseif events.subtype(1) == TRANSACTION_REQUEST_SPECTRUM
                mining_spectrum_active = 1;
                % Assign miner randomly
                assigned_miner = randi(length(miners));                 
                delay_mining = ComputeMiningTime(mu_spectrum);
                % Generate the block
                block = GenerateBlock(unconfirmed_spectrum_pool, ...
                    t, assigned_miner, MAX_BLOCK_SIZE_SPECTRUM, TRANSACTION_LENGTH, spectrum_block_id);    
                spectrum_bc = AddBlock(spectrum_bc, block, t);                       
                spectrum_bc.block_list(end).time_to_mine = delay_mining;               
                % Create an event for mining the block
                new_event = CreateEvent(EVENT_BLOCK_MINED, events.subtype(1), ...
                    t+delay_mining, assigned_miner, spectrum_block_id, UNDEFINED); 
                events = AddEvents(events, new_event); % Add new event to the queue
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'A spectrum block with id ' num2str(block.block_id) ' is ready to be mined']);
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Mining delay = ' num2str(delay_mining)]);
                % Store the information about the service
                for j = 1 : length(block.transaction_list)
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transactions ids: ' num2str(block.transaction_list(j).transaction_id)]);
                    spectrum_bc.block_list(end).transaction_list(j).timestamp_added_to_block = t;
                end
                % Reset the block timeout
                if events.type(1) ~= EVENT_BLOCK_TIMEOUT % Remove the timeout if not used
                    event_number = find(events.time == next_block_timeout_spectrum);                                
                    events = RemoveEvents(events,event_number);
                    % Create another timeout if there are unconfirmed transactions in the pool
                    if length(unconfirmed_spectrum_pool.transaction_count) > 1
                        next_block_timeout_spectrum = t + T_WAIT;                                    
                        timeout_event = CreateEvent(EVENT_BLOCK_TIMEOUT, events.subtype(1), ...
                            next_block_timeout_spectrum, source_transaction, UNDEFINED, spectrum_counter);
                        events = AddEvents(events, timeout_event);
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Timeout for generating a block: ' num2str(timeout_event.time)]);
                    end                                 
                end            
                % Increase the block id count
                spectrum_block_id = spectrum_block_id + 1;  
            end
            
            % Remove the current event from the list
            events = RemoveEvents(events, 1);

        %%%% BLOCK MINED: A block has been mined
        case EVENT_BLOCK_MINED % 3
            
            % Append mined block to blockchain to confirm the included transactions 
            miner_id = events.source_id(1);             % Check miner to find the block to be mined
            block_number = events.transaction_id(1);    % Get block number 
            % Write logs
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'A block with id ' num2str(block_number) ...
                ' has been mined by miner ' num2str(miner_id)]); 
            
            if sum(forked_blocks == block_number) == 0
                % Differentiate between service and spectrum BCs to store the time at which each transaction has been mined
                if events.subtype(1) == TRANSACTION_REQUEST_SERVICE                                         
                    % FIND BLOCK IX in list
                    ix_mined_block = FindIndexCandidateBlock(service_bc.block_list, miner_id, block_number);
                    if ix_mined_block > 0 
                        num_transactions = service_bc.block_list(ix_mined_block).num_transactions;
                        for i = 1 : service_bc.block_list(ix_mined_block).num_transactions
                           service_bc.block_list(ix_mined_block).transaction_list(i).timestamp_mined = t;
                        end       
                        service_bc.block_list(ix_mined_block).timestamp_mined = t;
                    end
                elseif events.subtype(1) == TRANSACTION_REQUEST_SPECTRUM   
                    % FIND BLOCK IX in list
                    ix_mined_block = FindIndexCandidateBlock(spectrum_bc.block_list, miner_id, block_number);
                    if ix_mined_block > 0 
                        num_transactions = spectrum_bc.block_list(ix_mined_block).num_transactions;
                        for i = 1 : spectrum_bc.block_list(ix_mined_block).num_transactions
                           spectrum_bc.block_list(ix_mined_block).transaction_list(i).timestamp_mined = t;
                        end                             
                        spectrum_bc.block_list(ix_mined_block).timestamp_mined = t;
                    end
                end                             
                if ix_mined_block > 0
                    % Compute the delay for propagating the mined block
                    delay_p2p = ComputeDelay(deployment, WIFI, P2P_NETWORK, miner_id, ...
                        HEADER_LENGTH + (TRANSACTION_LENGTH * num_transactions), INTERFERENCE_MODE);
                    % Create an event for propagating the mined block (services become effective)
                    new_event = CreateEvent(EVENT_MINED_BLOCK_PROPAGATED, events.subtype(1), ...
                        t + delay_p2p, miner_id, block_number, UNDEFINED);
                    events = AddEvents(events, new_event);
                    % Write logs
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'Propagating the service block in ' num2str(delay_p2p) ' s']);    
                end
            end
            % Remove the current event
            events = RemoveEvents(events, 1);

        %%%% BLOCK PROPAGATED
        % Block propagated 
        case EVENT_MINED_BLOCK_PROPAGATED % 4
            
            % _-------------____-------______--______----__-__-_-_-__
            block_number = events.transaction_id(1);
            miner_id = events.source_id(1);
            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'A mined block with ID ' ...
                num2str(block_number) ' has been propagated (sent by miner ' num2str(miner_id) ')']);   
            % Select the corresponding BC
            % + SERVICE BC
            if events.subtype(1) == TRANSACTION_REQUEST_SERVICE || events.subtype(1) == TRANSACTION_SERVICE_BID
                % Set mining to 0 (mining is finished)
                mining_active = 0;            
                [ix_block, occurrences] = FindIndexCandidateBlock(service_bc.block_list, miner_id, block_number);
                candidate_block = service_bc.block_list(ix_block);
                if ~isempty(occurrences) % FORK                    
                    if isempty(find(forked_blocks == service_bc.block_list(ix_block).block_id))
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL1 'Fork detected with service block ' num2str(block_number)]);
                        % Add block id to list of forked blocks
                        forked_blocks = [forked_blocks service_bc.block_list(ix_block).block_id]; 
                        service_bc.num_forks = service_bc.num_forks + 1;
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Transactions from forked service block:']);                                                                   
                        for i = 1 : length(service_bc.block_list(ix_block).transaction_list)  
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'ID: ' num2str(service_bc.block_list(ix_block).transaction_list(i).transaction_id)]); 
                        end                        
                    else
                        % Do nothing
                    end
                else % NO FORK
                    % Append the valid block to the blockchain
                    service_bc = MineBlock(service_bc, candidate_block, t);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Number of transactions in the service block: ' ...
                        num2str(service_bc.mined_block_list(end).num_transactions)]);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Transactions in the service block:']);    
                    for i = 1 : service_bc.mined_block_list(end).num_transactions
                       WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transaction ' num2str(service_bc.mined_block_list(end).transaction_list(i).transaction_id) ...
                           ': type ' num2str(service_bc.mined_block_list(end).transaction_list(i).type) ...
                           ' - source ' num2str(service_bc.mined_block_list(end).transaction_list(i).source_id) ...
                           ' - dest. ' num2str(service_bc.mined_block_list(end).transaction_list(i).dest_id)]); 
                       service_bc.mined_block_list(end).transaction_list(i).timestamp_confirmed = t;
                       % Process each transaction individually
                       transaction_aux = service_bc.mined_block_list(end).transaction_list(i);
                       transaction_type = transaction_aux.type;
                       origin_transaction_id = transaction_aux.source_transaction_id;  
                       % Differentiate between requests and bids
                       % + REQUESTS
                       if transaction_type == TRANSACTION_REQUEST_SERVICE
                           % Create bids from operators
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Generating bids from operators']);        
                           bids = GenerateBids(transaction_type, operators, transaction_aux);
                           % WAY TO SUBMIT BIDS (INDIVIDUALLY OR TOGETHER)
                           if bid_submission_mode == SUBMIT_BIDS_INDIVIDUALLY
                               % Submit each bid as a transaction to the blockchain
                               for j = 1 : length(bids)
                                   % Create the transaction
                                   new_transaction = GenerateTransaction(TRANSACTION_SERVICE_BID, ...
                                       bids(j).seller, transaction_aux.source_id, t, transaction_counter, ...
                                       transaction_aux.transaction_id, UNDEFINED); 
                                   new_transaction.contract = bids(j);                           
                                   % Add the new transaction to the pool of unconfirmed transactions
                                   WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Adding transaction (id ' ...
                                       num2str(new_transaction.transaction_id) ') to temp pool (current pool size: ' ...
                                       num2str(length(temp_transaction_pool.transaction_list)) ')' ]);            
                                   temp_transaction_pool = AddTransaction(temp_transaction_pool, new_transaction, 0, t, 0);                                     
                                   % Compute the delay to reach the P2P network (propagate the transaction)
                                   delay_p2p = ComputeDelay(deployment, WIFI, P2P_NETWORK, bids(j).seller, ...
                                       HEADER_LENGTH + (TRANSACTION_LENGTH * num_transactions), INTERFERENCE_MODE);
                                   % Schedule a new event to propagate the transaction
                                   new_event = CreateEvent(EVENT_TRANSACTION_PROPAGATED, ...
                                       TRANSACTION_SERVICE_BID, t + delay_p2p, ...
                                       bids(j).seller, new_transaction.transaction_id, service_counter);
                                   events = AddEvents(events, new_event);  
                                   WriteLogs(LOGS_ENABLED, logs_file, t + delay_p2p, [LOGS_LVL3 'Time to reach P2P network: ' num2str(new_event.time)]);
                                   % Increase transactions counter
                                   transaction_counter = transaction_counter + 1;
                               end  
                           % + BIDS
                           elseif bid_submission_mode == SUBMIT_BIDS_TOGETHER
                                % Create the transaction
                                new_transaction = GenerateTransaction(TRANSACTION_SERVICE_BID, ...
                                   UNDEFINED, transaction_aux.source_id, t, transaction_counter, ...
                                   transaction_aux.transaction_id, UNDEFINED); 
                                new_transaction.contract = bids;                           
                                % Add the new transaction to the pool of unconfirmed transactions
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Adding transaction (id ' ...
                                   num2str(new_transaction.transaction_id) ') to temp pool (current pool size: ' ...
                                   num2str(length(temp_transaction_pool.transaction_list)) ')' ]);            
                                temp_transaction_pool = AddTransaction(temp_transaction_pool, new_transaction, 0, t, 0);  
                                % Compute the delay to reach the P2P network (propagate the transaction)
                                delay_p2p = ComputeDelay(deployment, WIFI, P2P_NETWORK, 1, ...
                                   HEADER_LENGTH + (TRANSACTION_LENGTH * num_transactions), INTERFERENCE_MODE);
                                % Schedule a new event to propagate the transaction
                                new_event = CreateEvent(EVENT_TRANSACTION_PROPAGATED, ...
                                   TRANSACTION_SERVICE_BID, t + delay_p2p + rand()/10000, ...
                                   1, new_transaction.transaction_id, service_counter);
                                events = AddEvents(events, new_event);  
                                WriteLogs(LOGS_ENABLED, logs_file, t + delay_p2p, [LOGS_LVL3 'Time to reach P2P network: ' num2str(new_event.time)]);
                                % Increase transactions counter
                                transaction_counter = transaction_counter + 1;
                           end
                       elseif transaction_type == TRANSACTION_SERVICE_BID
                           [block_number, transaction_ix] = FindTransactionInBlockchain(service_bc, transaction_aux.source_transaction_id);  
                           % Perform reverse-auction to provide service
                           user_to_be_served = service_bc.mined_block_list(block_number).transaction_list(transaction_ix).source_id;
                           source_transaction_id = transaction_aux.transaction_id;
                           service_duration = transaction_aux.contract.service_duration;
                           service_price = transaction_aux.contract.price;
                           %disp(['Transaction ' num2str(source_transaction_id) ': Service to be provided to ' num2str(user_to_be_served)])                                
                           auction_winner = ServiceAuction(deployment, transaction_aux, operators, users, SERVICE_AUCTION_MODE);
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'A service has started']);                                              
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Requested by user ' num2str(user_to_be_served)]);
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Source transaction id: ' num2str(source_transaction_id)]);  
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Winner operator: ' num2str(auction_winner)]);   
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Duration: ' num2str(service_duration) ' s']);
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Price: ' num2str(service_price)]);
                           %if isempty(transaction_aux.contract.offered_service), transaction_aux.contract.offered_service = 0; end
                           best_ap = deployment.staNearestAp(user_to_be_served);                             
                           operators(auction_winner).subscribed_users{best_ap} = ...
                               [operators(auction_winner).subscribed_users{best_ap} user_to_be_served];
                           operators(auction_winner).users_requirements{best_ap} = ...
                               [operators(auction_winner).users_requirements{best_ap} transaction_aux.contract.offered_service];
                           operators(auction_winner).users_service_time{best_ap} = ...
                               [operators(auction_winner).users_service_time{best_ap} t+service_duration];
                           price_sta(user_to_be_served) = operators(auction_winner).min_price_per_unit;
                           if deployment.apStas(user_to_be_served) == UNDEFINED
                               best_channel = UNDEFINED;
                           else
                               best_channel = deployment.channelAps(deployment.apStas(user_to_be_served));   
                           end             
                           % Update the status of the deployment
                           deployment = UpdateDeploymentStatus(deployment, ...
                               user_to_be_served, 1, best_channel, best_ap);
                           % Update the timestamp of the requesting transaction
                           service_bc.mined_block_list(block_number).transaction_list(transaction_ix).timestamp_served = t;
                           % Schedule the end of the service
                           new_event = CreateEvent(EVENT_SERVICE_FINISHED, UNDEFINED, ...
                                t + service_duration, user_to_be_served, source_transaction_id, UNDEFINED);   
                           events = AddEvents(events, new_event);
                           % Indicate that a new service has started (in order to update operators' needs)
                           new_service_started = 1;
                       end
                    end
                    % Remove the block from the list of unconfirmed blocks
                    service_bc = RemoveBlock(service_bc, ix_block);
                    % Remove transactions from the pool of unconfirmed transactions
                    transactions_to_remove = CheckTransactionsTobeRemoved(service_bc.mined_block_list(end), unconfirmed_transaction_pool);    
                    unconfirmed_transaction_pool = RemoveTransactions(unconfirmed_transaction_pool, ...
                        transactions_to_remove, TRANSACTION_LENGTH);     
                    % Check if mining is possible
                    if ~mining_active && unconfirmed_transaction_pool.total_size >= MAX_BLOCK_SIZE
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'Size of pool = ' num2str(unconfirmed_spectrum_pool.total_size)]);
                        % Create mining event
                        new_event = CreateEvent(EVENT_START_MINING, events.subtype(1), ...
                            t+rand()/100000, UNDEFINED, UNDEFINED, UNDEFINED);  
                        events = AddEvents(events, new_event);
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'New mining event scheduled for t = ' num2str(new_event.time)]); 
                    elseif  ~mining_active && unconfirmed_transaction_pool.total_size > 0 ...
                            && next_block_timeout == UNDEFINED
                            next_block_timeout = t + T_WAIT;                                    
                            timeout_event = CreateEvent(EVENT_BLOCK_TIMEOUT, events.subtype(1), ...
                                next_block_timeout, UNDEFINED, UNDEFINED, UNDEFINED);
                            events = AddEvents(events, timeout_event);
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Timeout for generating a block: ' num2str(timeout_event.time)]);
                    end   
                end
                
                % Check if there are replicas of the same block still being mined to cancel them
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Removing repeated unmined service blocks...']);         
                for i = length(service_bc.block_list):-1:1
                    if block_number == service_bc.block_list(i).block_id ...
                        && miner_id ~= service_bc.block_list(i).assigned_miner ...
                        && service_bc.block_list(i).timestamp_mined <= 0
                        % Remove unmined blocks with the same id
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Removed service block ' ...
                            num2str(service_bc.block_list(i).block_id) ' from miner ' ...
                            num2str(service_bc.block_list(i).assigned_miner)]);                                
                        service_bc = RemoveBlock(service_bc, i);                                 
                    end
                end     
                % Check if additional resources are needed by each MNO
                if new_service_started
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'Updating operators needs...']); 
                    operators = UpdateSpectrumNeeds(operators, deployment);                           
                    for op = 1 : length(operators)
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Operator ' num2str(op) ': ' num2str(operators(op).required_spectrum)]); 
                        if sum(operators(op).required_spectrum) > 0                                    
                           for ap = 1 : deployment.nAps % Generate a spectrum request per operator and AP
                               if operators(op).updated_needs(ap)
                                   WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'A new spectrum request was generated']); 
                                   new_event = CreateEvent(EVENT_NEW_TRANSACTION, ...
                                       TRANSACTION_REQUEST_SPECTRUM, t + rand()/10000, ...
                                       op, ap, operators(op).required_spectrum(ap)-operators(op).old_needs(ap));
                                   events = AddEvents(events, new_event);  
                               end
                           end
                        end
                    end
                    new_service_started = 0; % Set the variable to false
                end
                    
            % + SPECTRUM BC    
            elseif events.subtype(1) == TRANSACTION_REQUEST_SPECTRUM || events.subtype(1) == TRANSACTION_SPECTRUM_BID
                mining_spectrum_active = 0;  % Set mining to 0 (mining is finished)
                [ix_block, occurrences] = FindIndexCandidateBlock(spectrum_bc.block_list, miner_id, block_number);
                candidate_block = spectrum_bc.block_list(ix_block);                
                if ~isempty(occurrences) % FORK
                    if isempty(find(forked_blocks_spectrum == spectrum_bc.block_list(ix_block).block_id))
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL1 'Fork detected with spectrum block ' num2str(block_number)]);
                        % Add block id to list of forked blocks
                        forked_blocks_spectrum = [forked_blocks_spectrum spectrum_bc.block_list(ix_block).block_id]; 
                        spectrum_bc.num_forks = spectrum_bc.num_forks + 1;
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Transactions from forked spectrum block:']);                                                                   
                        for i = 1 : length(spectrum_bc.block_list(ix_block).transaction_list)  
                            WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'ID: ' num2str(spectrum_bc.block_list(ix_block).transaction_list(i).transaction_id)]); 
                        end
                    else
                        % Do nothing
                    end
                else
                    % Append the valid block to the blockchain
                    spectrum_bc = MineBlock(spectrum_bc, candidate_block, t);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Number of transactions in the spectrum block ' ...
                        num2str(spectrum_bc.mined_block_list(end).num_transactions)]);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Transactions in the spectrum block:']);   
                    % Process each transaction individually
                    for i = 1 : spectrum_bc.mined_block_list(end).num_transactions
                        transaction_aux = candidate_block.transaction_list(i); 
                        transaction_type = transaction_aux.type;
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Transaction ' num2str(transaction_aux.transaction_id) ...
                           ': type ' num2str(transaction_aux.type) ...
                           ' - source ' num2str(transaction_aux.source_id) ...
                           ' - dest. ' num2str(transaction_aux.dest_id)]);                       
                        spectrum_bc.mined_block_list(end).transaction_list(i).timestamp_confirmed = t;
                        % Differentiate between requests and bids
                        % + REQUESTS
                        if transaction_type == TRANSACTION_REQUEST_SPECTRUM
                           % Create bids from operators
                           WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Generating bids from operators']);        
                           bids = GenerateBids(transaction_type, operators, transaction_aux);
                           % Process the transaction
                           operator_to_be_served = transaction_aux.source_id;
                           source_transaction_id = transaction_aux.transaction_id;  
                           % WAY TO SUBMIT BIDS (INDIVIDUALLY OR TOGETHER)
                           if bid_submission_mode == SUBMIT_BIDS_INDIVIDUALLY
                               % Submit each bid as a transaction to the blockchain
                               for j = 1 : length(bids)
                                   % Create the transaction
                                   new_transaction = GenerateTransaction(TRANSACTION_SPECTRUM_BID, ...
                                       bids(j).seller, operator_to_be_served, t, transaction_counter, ...
                                       source_transaction_id, UNDEFINED); 
                                   new_transaction.contract = bids(j);                           
                                   % Add the new transaction to the pool of unconfirmed transactions
                                   WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Adding transaction (id ' ...
                                       num2str(new_transaction.transaction_id) ') to temp pool (current pool size: ' ...
                                       num2str(length(temp_transaction_pool.transaction_list)) ')' ]);            
                                   temp_transaction_pool = AddTransaction(temp_transaction_pool, new_transaction, 0, t, 0);                                     
                                   % Compute the delay to reach the P2P network (propagate the transaction)
                                   delay_p2p = ComputeDelay(deployment, WIFI, P2P_NETWORK, bids(j).seller, ...
                                       HEADER_LENGTH + (TRANSACTION_LENGTH * num_transactions), INTERFERENCE_MODE);
                                   % Schedule a new event to propagate the transaction
                                   new_event = CreateEvent(EVENT_TRANSACTION_PROPAGATED, ...
                                       TRANSACTION_SPECTRUM_BID, t + delay_p2p, ...
                                       bids(j).seller, new_transaction.transaction_id, service_counter);
                                   events = AddEvents(events, new_event);  
                                   WriteLogs(LOGS_ENABLED, logs_file, t + delay_p2p, [LOGS_LVL3 'Time to reach P2P network: ' num2str(new_event.time)]);
                                   % Increase transactions counter
                                   transaction_counter = transaction_counter + 1;
                               end  
                           % + BIDS
                           elseif bid_submission_mode == SUBMIT_BIDS_TOGETHER
                                % Create the transaction
                                new_transaction = GenerateTransaction(TRANSACTION_SPECTRUM_BID, ...
                                   UNDEFINED, operator_to_be_served, t, transaction_counter, ...
                                   source_transaction_id, UNDEFINED); 
                                new_transaction.contract = bids;                           
                                % Add the new transaction to the pool of unconfirmed transactions
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL5 'Adding transaction (id ' ...
                                   num2str(new_transaction.transaction_id) ') to temp pool (current pool size: ' ...
                                   num2str(length(temp_transaction_pool.transaction_list)) ')' ]);            
                                temp_transaction_pool = AddTransaction(temp_transaction_pool, new_transaction, 0, t, 0);  
                                % Compute the delay to reach the P2P network (propagate the transaction)
                                delay_p2p = ComputeDelay(deployment, WIFI, P2P_NETWORK, 1, ...
                                   HEADER_LENGTH + (TRANSACTION_LENGTH * num_transactions), INTERFERENCE_MODE);
                                % Schedule a new event to propagate the transaction
                                new_event = CreateEvent(EVENT_TRANSACTION_PROPAGATED, ...
                                   TRANSACTION_SPECTRUM_BID, t + delay_p2p + rand()/10000, ...
                                   1, new_transaction.transaction_id, service_counter);
                                events = AddEvents(events, new_event);  
                                WriteLogs(LOGS_ENABLED, logs_file, t + delay_p2p, [LOGS_LVL3 'Time to reach P2P network: ' num2str(new_event.time)]);
                                % Increase transactions counter
                                transaction_counter = transaction_counter + 1;
                           end
                       elseif transaction_type == TRANSACTION_SPECTRUM_BID
                            % Run spectrum auction
                            [auction_winner, spectrum_share, service_duration] = ...
                                SpectrumAuction(transaction_aux, operators, ...
                                ap_spectrum_requested, SPECTRUM_AUCTION_MODE); 
                            % Find transaction containing the spectrum request
                            [block_number, transaction_ix] = FindTransactionInBlockchain(spectrum_bc, ...
                                transaction_aux.source_transaction_id);  
                            operator_to_be_served = spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).source_id;
                            if auction_winner > 0
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'A spectrum lease has started in AP ' num2str(ap_spectrum_requested)]);
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Shared operator ' num2str(operator_to_be_served)]);
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Sharing operator ' num2str(auction_winner)]);
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Source transaction id: ' num2str(source_transaction_id)]);  
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Duration: ' num2str(service_duration) ' s']);
                                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Spectrum share: ' num2str(spectrum_share)]);
                                % Update ownership of spectrum resources
                                operators = UpdateSpectrumOwnership(operators, operator_to_be_served, auction_winner, ap_spectrum_requested, spectrum_share);
                                % Update information in the original transaction
                                spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).contract.operator = auction_winner;
                                spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).contract.spectrum_shared = spectrum_share;
                                % Update the timestamp of the requesting transaction
                                spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).timestamp_served = t;
                                % Schedule the end of the spectrum lease
                                new_event = CreateEvent(EVENT_SERVICE_FINISHED, TRANSACTION_SPECTRUM_DELIVERY, ...
                                    t+service_duration, auction_winner, transaction_aux.source_transaction_id, ap_spectrum_requested); %operator_to_be_served
                                events = AddEvents(events, new_event);                                         
                            end    
                        end
                    end  
                    % Remove the block from the list of unconfirmed blocks
                    spectrum_bc = RemoveBlock(spectrum_bc, ix_block);
                    % Remove transactions from the pool of unconfirmed transactions
                    transactions_to_remove = CheckTransactionsTobeRemoved(spectrum_bc.mined_block_list(end), unconfirmed_spectrum_pool);    
                    unconfirmed_spectrum_pool = RemoveTransactions(unconfirmed_spectrum_pool, ...
                        transactions_to_remove, TRANSACTION_LENGTH);
                    % Check if mining is possible
                    if ~mining_spectrum_active && unconfirmed_spectrum_pool.total_size >= MAX_BLOCK_SIZE_SPECTRUM 
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'Size of pool = ' num2str(unconfirmed_spectrum_pool.total_size)]);
                        % Create mining event
                        new_event = CreateEvent(EVENT_START_MINING, events.subtype(1), ...
                            t+rand()/100000, UNDEFINED, UNDEFINED, UNDEFINED);  
                        events = AddEvents(events, new_event);
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'New mining event scheduled for t = ' num2str(new_event.time)]); 
                    elseif ~mining_spectrum_active && unconfirmed_spectrum_pool.total_size > 0 ...
                        && next_block_timeout_spectrum == UNDEFINED
                        next_block_timeout_spectrum = t + T_WAIT;                                    
                        timeout_event = CreateEvent(EVENT_BLOCK_TIMEOUT, events.subtype(1), ...
                            next_block_timeout_spectrum, UNDEFINED, UNDEFINED, UNDEFINED);
                        events = AddEvents(events, timeout_event);
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Timeout for generating a block: ' num2str(timeout_event.time)]);
                    end 
                end
                % Check if there are replicas of the same block still being mined to cancel them
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Removing repeated unmined service blocks...']);         
                for i = length(spectrum_bc.block_list):-1:1
                    if block_number == spectrum_bc.block_list(i).block_id ...
                        && miner_id ~= spectrum_bc.block_list(i).assigned_miner ...
                        && spectrum_bc.block_list(i).timestamp_mined <= 0
                        % Remove unmined blocks with the same id
                        WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Removed service block ' ...
                            num2str(spectrum_bc.block_list(i).block_id) ' from miner ' ...
                            num2str(spectrum_bc.block_list(i).assigned_miner)]);                                
                        spectrum_bc = RemoveBlock(spectrum_bc, i);                                 
                    end
                end                         
            end      
            
            % Remove events of pending mined blocks (still being mined)
            mining_events = find(events.type==EVENT_BLOCK_MINED);
            same_block_number_events = find(events.transaction_id(mining_events)==block_number);
            delete_events = find(events.subtype(same_block_number_events)==TRANSACTION_REQUEST_SERVICE);
            events = RemoveEvents(events, mining_events(delete_events)); 
            if ~isempty(events)
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL4 'Removed events: ' num2str(delete_events)]); 
            end

            % _-------------____-------______--______----__-__-_-_-__   
            % Compute and store the throughput until this moment
            if t > previous_t
                [throughput_users, satisfaction_users, activation_users, period_durations, requests_users, load_aps, required_load_aps] = ...
                    UpdatePerformanceStatistics(deployment, operators, users, ue_required_spectrum, throughput_users, satisfaction_users, ... 
                    activation_users, period_durations, requests_users, load_aps, required_load_aps, deployment.activeStas, price_sta, t-previous_t); 
                previous_t = t; 
            end
            % _-------------____-------______--______----__-__-_-_-__
              
            % Store the spectrum ownership until this moment
            [spectrum_ownership, spectrum_periods] = ...
                UpdateSpectrumStatistics(spectrum_ownership, spectrum_periods, operators, t);                                                
            % Compute and store the performance until this moment
            if t > previous_t
                [throughput_users, satisfaction_users, activation_users, period_durations, requests_users, load_aps, required_load_aps] = ...
                    UpdatePerformanceStatistics(deployment, operators, users, ue_required_spectrum, throughput_users, satisfaction_users, ... 
                    activation_users, period_durations, requests_users, load_aps, required_load_aps, deployment.activeStas, price_sta, t-previous_t);                         
                previous_t = t;
            end                

            % Remove the current event from the list
            events = RemoveEvents(events, 1);

        %%%% SERVICE FINISHED: A service has finished    
        case EVENT_SERVICE_FINISHED
                       
            if events.subtype(1) == TRANSACTION_SPECTRUM_DELIVERY                
                % Get information from the transaction in the spectrum BC
                [block_number, transaction_ix] = FindTransactionInBlockchain(spectrum_bc, events.transaction_id(1));
                %j = events.service_ix(1);
                ap_return = spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).contract.ap_spectrum_requested;
                if spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).contract.spectrum_shared > 0 
                    buyer_op = spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).contract.operator;
                    seller_op = spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).source_id;
                    spectrum_return = spectrum_bc.mined_block_list(block_number).transaction_list(transaction_ix).contract.spectrum_shared;
                    % Update ownership of spectrum resources
                    operators = UpdateSpectrumOwnership(operators, buyer_op, seller_op, ap_return, spectrum_return);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'A spectrum lease has finished in AP ' num2str(ap_return)]);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Seller operator ' num2str(seller_op)]);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Buyer operator ' num2str(buyer_op)]);
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Source transaction id: ' num2str(transaction_ix)]);  
                    WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'Spectrum share: ' num2str(spectrum_return)]);
                end
                % Store the spectrum ownership until this moment
                [spectrum_ownership, spectrum_periods] = UpdateSpectrumStatistics(spectrum_ownership, spectrum_periods, operators, t); 
            else            
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL2 'A service has finished']);
                WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL3 'User: ' num2str(events.source_id(1))]);
            end
            % Update the status of the deployment
            deployment = UpdateDeploymentStatus(deployment, ...
                events.source_id(1), 0, UNDEFINED, UNDEFINED);
            % Compute and store the throughput until this moment
            if t > previous_t
                [throughput_users, satisfaction_users, activation_users, period_durations, requests_users, load_aps, required_load_aps] = ...
                    UpdatePerformanceStatistics(deployment, operators, users, ue_required_spectrum, throughput_users, satisfaction_users, ... 
                    activation_users, period_durations, requests_users, load_aps, required_load_aps, deployment.activeStas, price_sta, t-previous_t); 
                previous_t = t;
            end
            % Remove the current event from list
            events = RemoveEvents(events, 1);

        otherwise
            disp('Unknown EVENT!');
            exit

    end

    % Sort events to pick the next one in the next loop iteration  
    events = SortEvents(events);

end % end "while t < sim_time"
WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL1 'No more events to execute']);
t = sim_time;   % end the simulation

% Update final statistics
[throughput_users, satisfaction_users, activation_users, period_durations, requests_users, load_aps, required_load_aps] = ...
    UpdatePerformanceStatistics(deployment, operators, users, ue_required_spectrum, throughput_users, satisfaction_users, ... 
    activation_users, period_durations, requests_users, load_aps, required_load_aps, deployment.activeStas, price_sta, t-sum(period_durations)); 

WriteLogs(LOGS_ENABLED, logs_file, t, [LOGS_LVL1 'Simulation finished']);

stop_time = t;

% disp(' ')
% disp('---------------------')
% disp('SUMMARY SIM. RESULTS')
% disp('---------------------')
% disp(' ')
% %Mean time between blocks + dropped requests
% mean_time_bw_blocks = zeros(1,length(mining_times));
% for j = 1 : length(mining_times)
%     time_bw_blocks = zeros(1, length(mining_times));
%     for i = 1 : length(mining_times)
%         if i == 1
%             time_bw_blocks(i) = mining_times(1);
%         else
%             time_bw_blocks(i) = mining_times(i)-mining_times(i-1);
%         end
%     end
%     mean_time_bw_blocks(j) = mean(time_bw_blocks);
% end

% if ~FORKS_ENABLED, services_information(:,10) = zeros(size(services_information,1),1) ; end
% if ~FORKS_ENABLED, spectrum_information(:,11) = zeros(size(spectrum_information,1),1) ; end

save(['./output/output_' num2str(SERVICE_AUCTION_MODE) '_' num2str(SPECTRUM_AUCTION_MODE) ...
'_' num2str(LAMBDA) '_' num2str(T_WAIT) '_' num2str(MAX_BLOCK_SIZE) '_' num2str(length(operators))  '_' num2str(RAND_DEPL) '.mat']);

timeElapsed = toc;
disp(['     -> Execution time: ' num2str(timeElapsed) ' seconds'])

end