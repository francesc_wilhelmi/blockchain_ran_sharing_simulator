%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
clear
close all
clc

% Load constants
constants
conf_environment_sce1

% Generate the deployment
deployment = GenerateDeployment(nStas);
% Create MNOs (assign resources to BSs) and users
[deployment, operators] = CreateOperators(deployment);
users = GenerateUsers(deployment);
% Determine miners and computational power
[deployment, miners] = InitializeMiners(deployment);   

%if PLOTS_ENABLED, DrawDeployment(deployment); end
if LOGS_ENABLED, PrintSimulationDetails(deployment, operators, users); end
%%
% Iterate for each value of user activity (number of requests per second)
for a = 1 : length(service_auction_modes)
    disp(['Service auction type = ' num2str(service_auction_modes(a))])
    for aa = 1 : length(spectrum_auction_modes)
        disp([' . Spectrum auction type = ' num2str(spectrum_auction_modes(aa))])
        for l = 1 : length(lambda)
            % Iterate for each value of user activity (number of requests per second)
            disp(['   + Users arrivals (lambda) = ' num2str(lambda(l))])
            % Iterate for each block timeout
            for t = 1 : length(block_timeout)
                disp(['     * Block timeout = ' num2str(block_timeout(t))])
                % Iterate for each block size
                for s = 1 : length(block_size)    
                    MAX_BLOCK_SIZE = TRANSACTION_LENGTH*block_size(s);       % Maximum block size in bits                
                    % RUN THE SIMULATION
                    RunSimulation(deployment, operators, users, miners, service_auction_modes(a), ...
                        spectrum_auction_modes(aa), lambda(l), block_timeout(t), TRANSACTION_LENGTH*block_size(s), 1);                      
                end % end "for" block size values
            end % end "for" timer values      
        end % end "for" lambda values  
    end % end "for" spectrum auction modes    
end % end "for" service auction modes  

%% Plot results
if PLOTS_ENABLED
    PlotResults(service_auction_modes, spectrum_auction_modes, lambda, block_timeout, TRANSACTION_LENGTH.*block_size);
end