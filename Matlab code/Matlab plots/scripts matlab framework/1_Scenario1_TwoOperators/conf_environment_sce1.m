%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************

%%% File description: script for storing the configuration of the deployment

% Enable/disable plots and logs
PLOTS_ENABLED = 1;
LOGS_ENABLED = 0;

FORKS_ENABLED = 0;  % Enable/disable Forks
RETRANSMISSIONS_ENABLED = 0; % Enable/disable retransmissions

% Set simulation parameters
sim_time = 300;      % simulation time
service_auction_modes = [AUCTION_RANDOM_SELECTION];% AUCTION_RANDOM_SELECTION];
spectrum_auction_modes = [AUCTION_STATIC AUCTION_DYNAMIC];
block_timeout = [5];   % timeout in seconds for generating a block
block_size = 5;      % block size in number of transactions
queue_length = 1000;   % number of transactions that fit the queue
lambda = [10];         %[5 7.5 10 12.5 15];           % arrivals rate (UE requests)

% Deployment characteristics
nRings = 2;     % Number of rings in the deployment (max=2)
R = 10;         % Cell radius
nStas = 200;    % Number of STAs
nOperators = 2; % Number of operators

ownedApsRatio = [1 0]; % Ratio of APs owned by each operator

% Planning mode for allocating resources to BSs
%   * WIFI_SINGLE_CHANNEL_RANDOM = 1;
%   * WIFI_SINGLE_CHANNEL_ALL_SAME = 2;
PLANNING_MODE = 1; 

% Interference mode
INTERFERENCE_MODE = 1;  % 0-Real, 1-Worst-case

% Generic PHY modeling constants
PATH_LOSS_MODEL = 1;                % Path loss model index
NUM_CHANNELS_SYSTEM = 1;            % Maximum allowed number of channels for a single transmission

% Approach for selecting opeartors in the service auction
% - AUCTION_NEAREST_AP = 1;
% - AUCTION_LOWEST_PRICE = 2;
% - AUCTION_WEIGHTED_SUM = 3; 
% - AUCTION_RANDOM_SELECTION = 4; 
operator_selection_approach = AUCTION_NEAREST_AP;
    
% Approach for leasing spectrum in the operators auction
% - AUCTION_STATIC
% - AUCTION_DYNAMIC
spectrum_leasing_approach = AUCTION_STATIC;

% Users' behavior
deltaActivationProbability = 0.01;  % Interval between discretized steps
minServiceDuration = 10;             % Minimum service duration in seconds
maxServiceDuration = 60;            % Maximum service duration in seconds
deltaServiceDuration = .5;          % Interval between discretized steps
minThroughputReq = 0.001;           % Minimum throughput required in bps
maxThroughputReq = 0.02;            % Maximum throughput required in bps
deltaThroughputReq = 0.005;         % Interval between discretized steps
minDelayReq = 0.1;                  % Minimum delay required in seconds
maxDelayReq = 5;                    % Maximum delay required in seconds
deltaDelayReq = 0.1;                % Interval between discretized steps

% Blockchain
MINING_DIFFICULTY = 1;      % Mining difficulty
TRANSACTION_LENGTH = 3000;  % Length of a transaction in bits
HEADER_LENGTH = 0;%640;        % Length of a transaction in bits
mu = 10;

maxPricePerServiceUnit = 1;
minPricePerServiceUnit = 1;
sellerAttitude = 1;

save('./tmp/conf_environment.mat');  % Save constants into the current folder