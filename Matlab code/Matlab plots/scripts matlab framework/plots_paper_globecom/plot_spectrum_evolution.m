lambda_boss = [10];
block_size_boss = 5;
path_1 = {'1_two_mno_1_0/1_0', '1_two_mno_1_0/05_05'}; % 
for p = 1 : length(path_1)
for a = 4 : 4
    for aa = 1 : 2            
        for l = 1 : 1
            for ti = 1 : 1               
                for s = 1 : 1
                    BLOCK_SIZE = TRANSACTION_LENGTH.*block_size_boss(s);
                    % Load results from simulations and prepare data to be plotted
                    load(['./output/' path_1{p} '/output_' num2str(a) '_' ...
                        num2str(aa) '_' num2str(lambda_boss(l)) '_' ...
                        num2str(block_timeout(ti)) '_' num2str(BLOCK_SIZE) '_2_1.mat'])
                                                    
                    for i = 1 : length(spectrum_ownership)
                        agg_spectrum_per_operator{p,aa}(i,:) = sum(spectrum_ownership{i}');
                    end           
                    
                end
            end
        end
        
        % Distribute results over time periods of the same length
        nSamples = 100;
        cum_time = cumsum(spectrum_periods);
        for t = 1 : nSamples
            samples_period = [];
            for i = 1 : length(spectrum_periods)
                if (t < cum_time(i) && cum_time(i) < t+(sim_time/nSamples)) %|| ...
                    %(t < service_start{k,m}(i)+service_duration{k,m}(i) && ...
                    %    service_start{k,m}(i)+service_duration{k,m}(i) < t+(sim_time/nSamples)) 
                    samples_period = [samples_period i];
                end
            end
            for i = 1 : length(spectrum_ownership)
                if isempty(samples_period)
                    spectrum_distribution{p,aa}(t,i) = spectrum_distribution{aa}(t-1,i);
                else
                    spectrum_distribution{p,aa}(t,i) = mean(agg_spectrum_per_operator{aa}(i,samples_period));
                end
            end
        end

    end
end
end

%%
figure
for a = 1 : 2
%     subplot(1,2,a)
    for i = 1 : length(agg_spectrum_per_operator)
%         if a == 1 && i == 1
%             plot((10/19)*ones(1,length(agg_spectrum_per_operator{2})), 'linewidth', 2.0);
%         elseif a == 1 && i == 2
%             plot((9/19)*ones(1,length(agg_spectrum_per_operator{2})), 'linewidth', 2.0);
%         else
            plot((1/19).*agg_spectrum_per_operator{a,2}(i,:)', 'linewidth', 2.0);
%         end
    hold on
    set(gca,'FontSize',18,'FontName','Times')
    end
end
axis([0 length(agg_spectrum_per_operator{a}) 0 1.1])
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
xlabel('Sim. time (s)')
ylabel('Spectrum share')
legend({'MNO_1 (static)', 'MNO_2 (static)', 'MNO_1 (dynamic)', 'MNO_2 (dynamic)'})

% axis([0 nSamples 0 1.1*max(aggregate_ue_perf{2}/1e6)])
% xlabel('Time (s)')
% ylabel('Capacity (Mbps)')
% yyaxis right
% plot(mean_ue_satisfaction{1}, 'r-o', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_satisfaction{2}))
% hold on
% plot(mean_load_operators{1}, 'r--x', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_load_operators{1})) 
% axis([0 nSamples 0 1])
% %plot(ones(1, nSamples), 'k--', 'linewidth', 1.0)
% grid on
% grid minor
% set(gca,'FontSize',18,'FontName','Times')
% ylabel('Satisfaction / Load')
% legend({'Agg. UE capacity', 'Mean UE satisfaction', 'Mean BS load'})
% 
% subplot(1,2,2)
% plot(aggregate_ue_perf{2}/1e6, 'b-s', 'linewidth', 2.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(aggregate_ue_perf{2}))
% hold on
% axis([0 nSamples 0 1.1*max(aggregate_ue_perf{2}/1e6)])
% xlabel('Time (s)')
% ylabel('Capacity (Mbps)')
% yyaxis right
% plot(mean_satisfaction{2}, 'r-o', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_satisfaction{2}))
% hold on
% plot(mean_load_operators{2}, 'r--x', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_load_operators{2})) 
% axis([0 nSamples 0 1])
% grid on
% grid minor
% set(gca,'FontSize',18,'FontName','Times')
% ylabel('Satisfaction / Load')
% legend({'Agg. UE capacity', 'Mean UE satisfaction', 'Mean BS load'})