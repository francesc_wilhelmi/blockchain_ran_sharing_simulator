%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************

%%% File description: script for storing the configuration of the deployment

% Deployment characteristics
nRings = 2;     % Number of rings in the deployment (max=2)
R = 10;         % Cell radius
nStas = 200;    % Number of STAs
nOperators = 2; % Number of operators

% Planning mode for allocating resources to BSs
%   * WIFI_SINGLE_CHANNEL_RANDOM = 1;
%   * WIFI_SINGLE_CHANNEL_ALL_SAME = 2;
PLANNING_MODE = 1; 

% Interference mode
INTERFERENCE_MODE = 1;  % 0-Real, 1-Worst-case

% Generic PHY modeling constants
PATH_LOSS_MODEL = 1;                % Path loss model index
NUM_CHANNELS_SYSTEM = 1;            % Maximum allowed number of channels for a single transmission

% Users' behavior
deltaActivationProbability = 0.01;  % Interval between discretized steps
minServiceDuration = 5;             % Minimum service duration in seconds
maxServiceDuration = 10;            % Maximum service duration in seconds
deltaServiceDuration = .5;          % Interval between discretized steps
minThroughputReq = 0.5e6;           % Minimum throughput required in bps
maxThroughputReq = 20e6;            % Maximum throughput required in bps
deltaThroughputReq = 0.5e6;         % Interval between discretized steps
minDelayReq = 0.1;                  % Minimum delay required in seconds
maxDelayReq = 5;                    % Maximum delay required in seconds
deltaDelayReq = 0.1;                % Interval between discretized steps

% Blockchain
MINING_DIFFICULTY = 1;      % Mining difficulty
TRANSACTION_LENGTH = 3000;  % Length of a transaction in bits
HEADER_LENGTH = 640;        % Length of a transaction in bits
mu = 10;

maxPricePerServiceUnit = 1;
minPricePerServiceUnit = 1;
sellerAttitude = 1;

save('./tmp/conf_environment.mat');  % Save constants into the current folder